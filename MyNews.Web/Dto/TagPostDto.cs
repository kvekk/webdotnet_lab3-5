﻿using System.ComponentModel.DataAnnotations;

namespace MyNews.Web.Dto;

public class TagPostDto
{
    [MaxLength(40, ErrorMessage = "Tag name cannot be longer than 40 characters")]
    public string Name { get; set; } = string.Empty;
}