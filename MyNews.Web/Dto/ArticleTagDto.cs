﻿using MyNews.DAL.Entities;

namespace MyNews.Web.Dto
{
    public class ArticleTagDto
    {
        public TagResultDto Tag { get; set; }
    }
}
