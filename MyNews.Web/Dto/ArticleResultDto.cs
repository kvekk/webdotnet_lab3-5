﻿using MyNews.DAL.Entities;
using System.ComponentModel.DataAnnotations;

namespace MyNews.Web.Dto;

public class ArticleResultDto
{
    public int ArticleId { get; set; }
    public string Headline { get; set; } = string.Empty;
    public string Text { get; set; } = string.Empty;
    public DateTime PublishDate { get; set; } = DateTime.Now;

    public CategoryResultDto Category { get; set; } = new();
    public UserResultDto User { get; set; } = new();

    public ICollection<ArticleTagDto>? ArticleTags { get; set; }
}
