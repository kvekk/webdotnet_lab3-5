﻿using System.ComponentModel.DataAnnotations;

namespace MyNews.Web.Dto
{
    public class ArticlePutDto
    {
        public int ArticleId { get; set; }
        [MaxLength(100, ErrorMessage = "Headline cannot be longer than 100 characters")]
        public string Headline { get; set; } = string.Empty;
        public string Text { get; set; } = string.Empty;
    }
}
