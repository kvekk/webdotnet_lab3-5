﻿namespace MyNews.Web.Dto;

public class TagResultDto
{
    public int TagId { get; set; }
    public string Name { get; set; } = string.Empty;
}