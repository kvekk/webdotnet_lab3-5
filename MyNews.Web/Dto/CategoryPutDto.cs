﻿using System.ComponentModel.DataAnnotations;

namespace MyNews.Web.Dto;

public class CategoryPutDto
{
    public int CategoryId { get; set; }

    [MaxLength(25, ErrorMessage = "Category name cannot be longer than 25 characters")]
    public string Name { get; set; } = string.Empty;
}
