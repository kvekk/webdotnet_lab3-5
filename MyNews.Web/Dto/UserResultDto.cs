﻿using System.ComponentModel.DataAnnotations;

namespace MyNews.Web.Dto;

public class UserResultDto
{
    public int UserId { get; set; }
    public string Username { get; set; } = string.Empty;
}
