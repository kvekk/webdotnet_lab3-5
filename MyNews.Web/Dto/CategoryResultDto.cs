﻿namespace MyNews.Web.Dto;

public class CategoryResultDto
{
    public int CategoryId { get; set; }
    public string Name { get; set; } = string.Empty;
}
