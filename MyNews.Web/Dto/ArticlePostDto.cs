﻿using MyNews.DAL.Entities;
using System.ComponentModel.DataAnnotations;

namespace MyNews.Web.Dto;

public class ArticlePostDto
{
    [MaxLength(100, ErrorMessage = "Headline cannot be longer than 100 characters")]
    public string Headline { get; set; } = string.Empty;
    public string Text { get; set; } = string.Empty;

    //public ICollection<ArticleTag> ArticleTags { get; set; } = null!;
}
