﻿using System.ComponentModel.DataAnnotations;

namespace MyNews.Web.Dto;

public class TagPutDto
{
    public int TagId { get; set; }
    
    [MaxLength(40, ErrorMessage = "Tag name cannot be longer than 40 characters")]
    public string Name { get; set; } = string.Empty;
}
