﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyNews.BLL.Services;
using MyNews.DAL.Entities;
using MyNews.Web.Dto;
using System.Security.Claims;

namespace MyNews.Web.Controllers;

[Route("api/articles")]
[ApiController]
public class ArticleController : ControllerBase
{
    private readonly IArticleService articleService;
    private readonly ICategoryService categoryService;
    private readonly IAuthService authService;
    private readonly IMapper mapper;

    public ArticleController(IArticleService articleService, ICategoryService categoryService,
        IAuthService authService, ITagService tagService, IMapper mapper)
    {
        this.articleService = articleService;
        this.categoryService = categoryService;
        this.authService = authService;
        this.mapper = mapper;
    }

    [HttpGet]
    public IActionResult GetAll()
    {
        var articles = articleService.GetAll();
        return Ok(mapper.Map<List<ArticleResultDto>>(articles));
    }

    [HttpGet("{id}")]
    public IActionResult Get(int id)
    {
        var article = articleService.Get(id);

        if (article == null)
        {
            return NotFound();
        }

        return Ok(mapper.Map<ArticleResultDto>(article));
    }

    [HttpGet("category")]
    public IActionResult GetByCategory([FromQuery] int categoryId)
    {
        var articles = articleService.FindByCategory(categoryId);
        return Ok(mapper.Map<List<ArticleResultDto>>(articles));
    }

    [HttpGet("tags")]
    public IActionResult GetByTags([FromQuery] params int[] tagIds)
    {
        var articles = articleService.FindByTags(tagIds);
        return Ok(mapper.Map<List<ArticleResultDto>>(articles));
    }

    [HttpGet("author")]
    public IActionResult GetByAuthor([FromQuery] string username)
    {
        var articles = articleService.FindByAuthor(username);
        return Ok(mapper.Map<List<ArticleResultDto>>(articles));
    }

    [HttpGet("date")]
    public IActionResult GetInBetweenDates([FromQuery] DateTime start, DateTime end)
    {
        var articles = articleService.FindByDate(start, end);
        return Ok(mapper.Map<List<ArticleResultDto>>(articles));
    }

    [HttpPost]
    [Authorize]
    public IActionResult Post([FromBody] ArticlePostDto articleDto, [FromQuery] int categoryId, [FromQuery] params int[] tags)
    {
        if (!ModelState.IsValid)
            return BadRequest();

        var category = categoryService.Get(categoryId);
        if (category == null)
        {
            return NotFound();
        }

        var article = mapper.Map<Article>(articleDto);
        try
        {
            article.Category = category;
            article.User = authService.Get(int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier)));
            articleService.Create(article);
            foreach (var tag in tags)
            {
                articleService.AddTag(article.ArticleId, tag);
            }
            return StatusCode(201, "Successfully created");
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    [HttpPut("{id}")]
    public IActionResult Put(int id, ArticlePutDto articleDto)
    {
        if (articleService.Get(id) == null)
            return NotFound();
        
        if (id != articleDto.ArticleId)
            return BadRequest();

        if (!ModelState.IsValid)
            return BadRequest();

        try
        {
            articleService.Update(mapper.Map<Article>(articleDto));
            return Ok(articleDto);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        var article = articleService.Get(id);

        if (article == null)
            return NotFound();

        try
        {
            articleService.Delete(id);
            return Ok("Successfully deleted");
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }
}
