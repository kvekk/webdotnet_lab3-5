﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MyNews.BLL.Services;
using MyNews.DAL.Entities;
using MyNews.Web.Dto;

namespace MyNews.Web.Controllers;

[Route("api/tags")]
[ApiController]
public class TagController : ControllerBase
{
    private readonly ITagService tagService;
    private readonly IMapper mapper;

    public TagController(ITagService tagService, IMapper mapper)
    {
        this.tagService = tagService;
        this.mapper = mapper;
    }

    [HttpGet]
    public IActionResult GetAll()
    {
        var tags = tagService.GetAll();
        return Ok(mapper.Map<List<TagResultDto>>(tags));
    }

    [HttpGet("{id}")]
    public IActionResult Get(int id)
    {
        var tag = tagService.Get(id);

        if (tag == null)
        {
            return NotFound();
        }

        return Ok(mapper.Map<TagResultDto>(tag));
    }

    [HttpPost]
    public IActionResult Post(TagPostDto tagDto)
    {
        if (!ModelState.IsValid)
            return BadRequest();

        var tag = mapper.Map<Tag>(tagDto);
        try
        {
            tagService.Create(tag);
            return StatusCode(201, "Successfully created");
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    [HttpPut("{id}")]
    public IActionResult Put(int id, TagPutDto tagDto)
    {
        if (id != tagDto.TagId)
            return BadRequest();
        
        if (!ModelState.IsValid)
            return BadRequest();

        try
        {
            tagService.Update(mapper.Map<Tag>(tagDto));
            return Ok(tagDto);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        var tag = tagService.Get(id);

        if (tag == null)
            return NotFound();

        tagService.Delete(id);
        return Ok("Successfully deleted");
    }
}
