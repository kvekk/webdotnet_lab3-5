﻿using Microsoft.AspNetCore.Mvc;
using MyNews.BLL.Services;
using MyNews.Web.Dto;

namespace MyNews.Web.Controllers;

[Route("api/auth")]
[ApiController]
public class AuthController : ControllerBase
{
    private readonly IAuthService authService;
    public AuthController(IAuthService authService)
    {
        this.authService = authService;
    }

    [HttpPost]
    [Route("register")]
    public IActionResult Register(UserRegisterDto user)
    {
        try
        {
            authService.Register(user.Username, user.Password);
            return Ok("Successfully registered!");
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    [HttpPost]
    [Route("login")]
    public IActionResult Login(UserLoginDto user)
    {
        try
        {
            string token = authService.Login(user.Username, user.Password);
            return Ok(new[] { "Successfully logged in!", token });
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }
}