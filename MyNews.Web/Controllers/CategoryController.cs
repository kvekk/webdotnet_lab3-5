﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MyNews.BLL.Services;
using MyNews.DAL.Entities;
using MyNews.Web.Dto;

namespace MyNews.Web.Controllers;

[Route("api/categories")]
[ApiController]
public class CategoryController : ControllerBase
{
    private readonly ICategoryService categoryService;
    private readonly IMapper mapper;

    public CategoryController(ICategoryService categoryService, IMapper mapper)
    {
        this.categoryService = categoryService;
        this.mapper = mapper;
    }

    [HttpGet]
    public IActionResult GetAll()
    {
        var categories = categoryService.GetAll();
        return Ok(mapper.Map<List<CategoryResultDto>>(categories));
    }

    [HttpGet("{id}")]
    public IActionResult Get(int id)
    {
        var category = categoryService.Get(id);

        if (category == null)
        {
            return NotFound();
        }

        return Ok(mapper.Map<CategoryResultDto>(category));
    }

    [HttpPost]
    public IActionResult Post([FromBody] CategoryPostDto categoryDto)
    {
        if (!ModelState.IsValid)
            return BadRequest();

        var category = mapper.Map<Category>(categoryDto);
        try
        {
            categoryService.Create(category);
            return StatusCode(201, "Successfully created");
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    [HttpPut("{id}")]
    public IActionResult Put(int id, CategoryPutDto categoryDto)
    {
        if (categoryService.Get(id) == null)
            return NotFound();
        
        if (id != categoryDto.CategoryId)
            return BadRequest();

        if (!ModelState.IsValid)
            return BadRequest();

        try
        {
            categoryService.Update(mapper.Map<Category>(categoryDto));
            return Ok(categoryDto);
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        var category = categoryService.Get(id);

        if (category == null)
            return NotFound();

        try
        {
            categoryService.Delete(id);
            return Ok("Successfully deleted");
        }
        catch (Exception e)
        {
            return BadRequest(e.Message);
        }
    }
}
