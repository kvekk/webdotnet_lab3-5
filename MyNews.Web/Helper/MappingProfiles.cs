﻿using AutoMapper;
using MyNews.DAL.Entities;
using MyNews.Web.Dto;

namespace MyNews.BLL.Helper;

public class MappingProfiles : Profile
{
    public MappingProfiles()
    {
        //CreateMap<Article, ArticleDto>();
        //CreateMap<Category, CategoryDto>();
        CreateMap<Category, CategoryResultDto>().ReverseMap();
        CreateMap<Category, CategoryPostDto>().ReverseMap();
        CreateMap<Category, CategoryPutDto>().ReverseMap();
        CreateMap<Tag, TagResultDto>().ReverseMap();
        CreateMap<Tag, TagPostDto>().ReverseMap();
        CreateMap<Tag, TagPutDto>().ReverseMap();
        
        //CreateMap<Article, ArticleResultDto>().ReverseMap();
        // map article and articleresultdto so that resultdto gets rid of articles in category
        CreateMap<User, UserResultDto>().ReverseMap();
        CreateMap<ArticleTag, ArticleTagDto>().ReverseMap();
        CreateMap<Article, ArticleResultDto>().ReverseMap();
        CreateMap<Article, ArticlePutDto>().ReverseMap();

        CreateMap<Article, ArticlePostDto>().ReverseMap();

       

        //CreateMap<Article, ArticlePostDto>().ReverseMap();
    }
}
