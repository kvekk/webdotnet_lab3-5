﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNews.BLL.Services;

public interface IService<T>
{
    ICollection<T> GetAll();
    T? Get(int id);
    void Create(T item);
    void Update(T item);
    void Delete(int id);
}
