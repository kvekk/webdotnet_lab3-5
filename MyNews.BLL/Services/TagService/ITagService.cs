﻿using MyNews.DAL.Entities;

namespace MyNews.BLL.Services;

public interface ITagService : IService<Tag>
{
    ICollection<Article> GetArticles(int id);
}
