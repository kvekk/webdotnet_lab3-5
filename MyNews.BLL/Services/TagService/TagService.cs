﻿using MyNews.DAL.Data;
using MyNews.DAL.Entities;

namespace MyNews.BLL.Services;

public class TagService : ITagService
{
    private readonly IUnitOfWork uof;

    public TagService(IUnitOfWork uof)
    {
        this.uof = uof;
    }
    
    public void Create(Tag item)
    {
        if (uof.TagRepository.Find(t => t.Name.ToLower() == item.Name.ToLower()).Any())
        {
            throw new InvalidOperationException("Cannot create a duplicate tag.");
        }
        uof.TagRepository.Create(item);
        uof.Commit();
    }

    public void Delete(int id)
    {
        uof.TagRepository.Delete(id);
        uof.Commit();
    }

    public void Update(Tag item)
    {
        if (uof.TagRepository.Find(c => c.Name == item.Name && c.TagId != item.TagId)
            .Any())
        {
            throw new InvalidOperationException("There is already a tag with such a name.");
        }

        uof.TagRepository.Update(item);
        uof.Commit();
    }

    public Tag? Get(int id)
    {
        return uof.TagRepository.Get(id);
    }

    public ICollection<Tag> GetAll()
    {
        return uof.TagRepository.GetAll();
    }

    public ICollection<Article> GetArticles(int id)
    {
        return uof.TagRepository.GetArticles(id);
    }
}