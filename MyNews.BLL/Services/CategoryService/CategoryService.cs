﻿using MyNews.DAL.Data;
using MyNews.DAL.Entities;

namespace MyNews.BLL.Services;

public class CategoryService : ICategoryService
{
    private readonly IUnitOfWork uof;

    public CategoryService(IUnitOfWork uof)
    {
        this.uof = uof;
    }
    
    public void Create(Category item)
    {
        if (uof.CategoryRepository.Find(c => c.Name.ToLower() == item.Name.ToLower()).Any())
        {
            throw new InvalidOperationException("Cannot create a duplicate category.");
        }
        uof.CategoryRepository.Create(item);
        uof.Commit();
    }

    public void Delete(int id)
    {
        if (GetArticles(id).Any())
        {
            throw new InvalidOperationException("Cannot delete category that has articles.");
        }
        else
        {
            uof.CategoryRepository.Delete(id);
            uof.Commit();
        }
    }

    public void Update(Category item)
    {
        if (uof.CategoryRepository.Find(c => c.Name == item.Name && c.CategoryId != item.CategoryId)
            .Any())
        {
            throw new InvalidOperationException("There is already a category with such a name.");
        }

        uof.CategoryRepository.Update(item);
        uof.Commit();
    }

    public Category? Get(int id)
    {
        return uof.CategoryRepository.Get(id);
    }

    public ICollection<Category> GetAll()
    {
        return uof.CategoryRepository.GetAll();
    }

    public ICollection<Article> GetArticles(int id)
    {
        return uof.CategoryRepository.GetArticles(id);
    }
}
