﻿using MyNews.DAL.Entities;

namespace MyNews.BLL.Services;

public interface ICategoryService : IService<Category>
{
    ICollection<Article> GetArticles(int id);
}
