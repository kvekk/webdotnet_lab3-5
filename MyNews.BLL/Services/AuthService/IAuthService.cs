﻿using MyNews.DAL.Entities;

namespace MyNews.BLL.Services;

public interface IAuthService
{
    void Register(string username, string password);
    string Login(string username, string password);
    User Get(int id);
}
