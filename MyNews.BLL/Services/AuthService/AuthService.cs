﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MyNews.DAL.Data;
using MyNews.DAL.Entities;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace MyNews.BLL.Services;

public class AuthService : IAuthService
{
    private readonly IUnitOfWork uof;
    private readonly IConfiguration configuration;

    public AuthService(IUnitOfWork uof, IConfiguration configuration)
	{
		this.uof = uof;
        this.configuration = configuration;
    }

    public User Get(int id)
    {
        var user = uof.AuthRepository.Get(id);
        if (user == null)
        {
            throw new InvalidOperationException("User not found.");
        }
        return user;
    }

	public void Register(string username, string password)
	{
        if (uof.AuthRepository.UserExists(username))
        {
            throw new ArgumentException("This username already exists.");
        }

        CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

        var user = new User
        {
            Username = username,
            PasswordHash = passwordHash,
            PasswordSalt = passwordSalt
        };
        
        uof.AuthRepository.Create(user);
        uof.Commit();
    }

    public string Login(string username, string password)
    {
        var user = uof.AuthRepository
            .Find(u => u.Username.ToLower().Equals(username.ToLower()))
            .FirstOrDefault();

        if (user == null)
        {
            throw new ArgumentException("User not found.");
        }
        else if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
        {
            throw new ArgumentException("Wrong password.");
        }
        else
        {
            return CreateToken(user);
        }
    }

    private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
    {
        using var hmac = new System.Security.Cryptography.HMACSHA512();
        passwordSalt = hmac.Key;
        passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
    }

    private static bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
    {
        using var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt);
        var computeHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
        return computeHash.SequenceEqual(passwordHash);
    }

    private string CreateToken(User user)
    {
        List<Claim> claims = new()
        {
            new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()),
            new Claim(ClaimTypes.Name, user.Username)
        };

        SymmetricSecurityKey key = new(System.Text.Encoding.UTF8.GetBytes(configuration.
            GetSection("AppSettings:Token").Value!));

        SigningCredentials creds = new(key, SecurityAlgorithms.HmacSha512Signature);

        SecurityTokenDescriptor tokenDescriptor = new()
        {
            Subject = new ClaimsIdentity(claims),
            Expires = DateTime.Now.AddDays(1),
            SigningCredentials = creds
        };

        JwtSecurityTokenHandler tokenHandler = new();
        SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
        
        return tokenHandler.WriteToken(token);
    }
}
