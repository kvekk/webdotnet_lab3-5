﻿using MyNews.DAL.Data;
using MyNews.DAL.Entities;

namespace MyNews.BLL.Services;

public class ArticleService : IArticleService
{
    private readonly IUnitOfWork uof;

    public ArticleService(IUnitOfWork uof)
    {
        this.uof = uof;
    }

    public void Create(Article item)
    {
        uof.ArticleRepository.Create(item);
        uof.Commit();
    }

    public void Delete(int id)
    {
        uof.ArticleRepository.Delete(id);
        uof.Commit();
    }
    
    public void Update(Article item)
    {
        uof.ArticleRepository.Update(item);
        uof.Commit();
    }

    public Article? Get(int id)
    {
        return uof.ArticleRepository.Get(id);
    }

    public ICollection<Article> GetAll()
    {
        return uof.ArticleRepository.GetAll().OrderByDescending(a => a.PublishDate).ToList();
    }

    public ICollection<Article> FindByAuthor(string author)
    {
        return uof.ArticleRepository.Find(a => a.User.Username.StartsWith(author));
    }

    public ICollection<Article> FindByCategory(int categoryId)
    {
        return uof.ArticleRepository
            .Find(a => a.Category.CategoryId == categoryId)
            .OrderByDescending(a => a.PublishDate)
            .ToList(); ;
    }

    public ICollection<Article> FindByDate(DateTime dateStart, DateTime dateEnd)
    {
        return uof.ArticleRepository.Find(a => a.PublishDate >= dateStart && a.PublishDate <= dateEnd);
    }

    public ICollection<Article> FindByTags(params int[] tags)
    {
        var articles = new HashSet<Article>();
        foreach (var tag in tags)
        {
            var articlesWithTag = uof.ArticleRepository.Find(a => a.ArticleTags.Any(at => at.TagId == tag));
            articles.UnionWith(articlesWithTag);
        }
        return articles;
    }
    
    public void AddTag(int articleId, int tagId)
    {
        var tag = uof.TagRepository.Get(tagId);
        if (tag == null)
        {
            return;
        }
        uof.ArticleRepository.AddTag(articleId, tagId);
        uof.Commit();
    }
    
    public ICollection<Tag> GetTags(int id)
    {
        return uof.ArticleRepository.GetTags(id);
    }
}
