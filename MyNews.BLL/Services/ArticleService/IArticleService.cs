﻿using MyNews.DAL.Entities;

namespace MyNews.BLL.Services;

public interface IArticleService : IService<Article>
{
    void AddTag(int articleId, int tagId);
    ICollection<Tag> GetTags(int id);
    ICollection<Article> FindByCategory(int categoryId);
    ICollection<Article> FindByTags(params int[] tags);
    ICollection<Article> FindByAuthor(string author);
    ICollection<Article> FindByDate(DateTime dateStart, DateTime dateEnd);
}
