﻿using System.ComponentModel.DataAnnotations;

namespace MyNews.DAL.Entities;

public class Category
{
    public int CategoryId { get; set; }
    
    [MaxLength(20)]
    public string Name { get; set; } = null!;

    public ICollection<Article>? Articles { get; set; }
}
