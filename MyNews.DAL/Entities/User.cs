﻿using System.ComponentModel.DataAnnotations;

namespace MyNews.DAL.Entities;

public class User
{
    public int UserId { get; set; }
    
    [MaxLength(15)]
    public string Username { get; set; } = null!;
    public byte[] PasswordHash { get; set; } = null!;
    public byte[] PasswordSalt { get; set; } = null!;
    public ICollection<Article>? Articles { get; set; }
}