﻿using System.ComponentModel.DataAnnotations;

namespace MyNews.DAL.Entities;

public class Tag
{
    public int TagId { get; set; }
    
    [MaxLength(20)]
    public string Name { get; set; } = null!;
    public ICollection<ArticleTag> ArticleTags { get; set; } = null!;
}
