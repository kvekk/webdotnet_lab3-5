﻿using System.ComponentModel.DataAnnotations;

namespace MyNews.DAL.Entities;

public class Article
{
    public int ArticleId { get; set; }
    [MaxLength(100)]
    public string Headline { get; set; } = null!;
    public string Text { get; set; } = null!;
    public DateTime PublishDate { get; set; } = DateTime.Now;

    public Category Category { get; set; } = null!;
    public User User { get; set; } = null!;

    public ICollection<ArticleTag> ArticleTags { get; set; } = null!;
}
