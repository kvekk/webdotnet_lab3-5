﻿
using Microsoft.EntityFrameworkCore;
using MyNews.DAL.Data;
using MyNews.DAL.Entities;

namespace MyNews.DAL.Repositories;

public class TagRepository : ITagRepository
{
    private readonly DataContext db;
    public TagRepository(DataContext db)
    {
        this.db = db;
    }

    public void Create(Tag tag)
    {
        db.Tags.Add(tag);
    }

    public void Delete(int id)
    {
        var tag = db.Tags.Find(id);
        if (tag != null)
        {
            db.Tags.Remove(tag);
        }
    }

    public void Update(Tag tag)
    {
        db.Entry(tag).State = EntityState.Modified;
    }

    public Tag? Get(int id)
    {
        return db.Tags.Find(id);
    }

    public ICollection<Tag> GetAll()
    {
        return db.Tags.ToList();
    }

    public ICollection<Tag> Find(Func<Tag, bool> predicate)
    {
        return db.Tags.AsNoTracking().Where(predicate).ToList();
    }

    public ICollection<Article> GetArticles(int id)
    {
        return db.ArticleTags.Where(at => at.TagId == id).Select(at => at.Article).ToList();
    }
}
