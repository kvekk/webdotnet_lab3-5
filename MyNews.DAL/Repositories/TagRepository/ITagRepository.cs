﻿using MyNews.DAL.Entities;

namespace MyNews.DAL.Repositories;

public interface ITagRepository : IRepository<Tag>
{
    ICollection<Article> GetArticles(int id);
}
