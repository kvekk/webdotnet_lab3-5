﻿
using Microsoft.EntityFrameworkCore;
using MyNews.DAL.Data;
using MyNews.DAL.Entities;

namespace MyNews.DAL.Repositories;

public class CategoryRepository : ICategoryRepository
{
    private readonly DataContext db;
    public CategoryRepository(DataContext db)
    {
        this.db = db;
    }
    
    public void Create(Category category)
    {
        db.Categories.Add(category);
    }

    public void Delete(int id)
    {
        var category = db.Categories.Find(id);
        if (category != null)
            db.Categories.Remove(category);
    }

    public void Update(Category category)
    {
        db.Entry(category).State = EntityState.Modified;
    }

    public Category? Get(int id)
    {
        return db.Categories.Find(id);
    }
    
    public ICollection<Category> GetAll()
    {
        return db.Categories.ToList();
    }

    public ICollection<Category> Find(Func<Category, bool> predicate)
    {
        return db.Categories.AsNoTracking().Where(predicate).ToList();
    }

    public ICollection<Article> GetArticles(int id)
    {
        return db.Articles.Where(a => a.Category.CategoryId == id).ToList();
    }
}
