﻿
using MyNews.DAL.Entities;

namespace MyNews.DAL.Repositories;

public interface ICategoryRepository : IRepository<Category>
{
    ICollection<Article> GetArticles(int id);
}
