﻿namespace MyNews.DAL.Repositories;

public interface IRepository<T>
{
    ICollection<T> GetAll();
    T? Get(int id);
    ICollection<T> Find(Func<T, bool> predicate);
    void Create(T item);
    void Update(T item);
    void Delete(int id);
}