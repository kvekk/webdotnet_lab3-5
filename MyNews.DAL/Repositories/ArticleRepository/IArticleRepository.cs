﻿using MyNews.DAL.Entities;

namespace MyNews.DAL.Repositories;

public interface IArticleRepository : IRepository<Article>
{
    //Article? GetArticleById(int id);
    //ICollection<Article> GetArticlesByRecency();
    //ICollection<Article> GetArticlesByCategory(int categoryId);
    //ICollection<Article> GetArticlesByAuthor(string authorUsername);
    //ICollection<Article> GetArticlesInBetween(DateTime startDate, DateTime endDate);
    //ICollection<Article> GetArticlesByTags(params string[] tags);
    //bool ArticleExists(int id);
    void AddTag(int articleId, int tagId);
    ICollection<Tag> GetTags(int id);
}
