﻿
using Microsoft.EntityFrameworkCore;
using MyNews.DAL.Data;
using MyNews.DAL.Entities;

namespace MyNews.DAL.Repositories;

public class ArticleRepository : IArticleRepository
{
    private readonly DataContext db;
    public ArticleRepository(DataContext db)
    {
        this.db = db;
    }

    public void Create(Article article)
    {
        db.Articles.Add(article);
    }

    public void Delete(int id)
    {
        var article = db.Articles.Find(id);
        if (article != null)
            db.Articles.Remove(article);
    }

    public void Update(Article article)
    {
        db.Entry(article).State = EntityState.Modified;
    }

    public Article? Get(int id)
    {
        return db.Articles
            .Include(a => a.Category)
            .Include(a => a.User)
            .Include(a => a.ArticleTags)
            .ThenInclude(at => at.Tag)
            .FirstOrDefault(a => a.ArticleId == id);
    }

    public ICollection<Article> GetAll()
    {
        return db.Articles
            .Include(a => a.Category)
            .Include(a => a.User)
            .Include(a => a.ArticleTags)
            .ThenInclude(at => at.Tag)
            .ToList();
    }

    public ICollection<Article> Find(Func<Article, bool> predicate)
    {
        return db.Articles.AsNoTracking()
            .Include(a => a.Category)
            .Include(a => a.User)
            .Include(a => a.ArticleTags)
            .ThenInclude(at => at.Tag)
            .Where(predicate)
            .ToList();
    }

    public ICollection<Tag> GetTags(int id)
    {
        return db.Tags.Where(t => t.ArticleTags.Any(at => at.ArticleId == id)).ToList();
    }
    
    public void AddTag(int articleId, int tagId)
    {
        var articleTag = new ArticleTag { ArticleId = articleId, TagId = tagId };
        db.ArticleTags.Add(articleTag);
    }
}