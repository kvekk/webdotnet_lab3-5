﻿using Microsoft.EntityFrameworkCore;
using MyNews.DAL.Data;
using MyNews.DAL.Entities;

namespace MyNews.DAL.Repositories;

public class AuthRepository : IAuthRepository
{
    private readonly DataContext db;
    public AuthRepository(DataContext db)
    {
        this.db = db;
    }

    public void Create(User user)
    {
        db.Users.Add(user);
    }

    public void Delete(int id)
    {
        var user = db.Users.Find(id);
        if (user != null)
            db.Users.Remove(user);
    }

    public void Update(User user)
    {
        db.Entry(user).State = EntityState.Modified;
    }

    public User? Get(int id)
    {
        return db.Users.Find(id);
    }

    public ICollection<User> GetAll()
    {
        return db.Users.ToList();
    }

    public ICollection<User> Find(Func<User, bool> predicate)
    {
        return db.Users.Where(predicate).ToList();
    }
    public bool UserExists(string username)
    {
        return db.Users.Any(u => u.Username.ToLower() == username.ToLower());
    }
}
