﻿using MyNews.DAL.Entities;

namespace MyNews.DAL.Repositories;

public interface IAuthRepository : IRepository<User>
{
    bool UserExists(string username);
}
