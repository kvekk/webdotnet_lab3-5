﻿using MyNews.DAL.Repositories;

namespace MyNews.DAL.Data;

public class UnitOfWork : IUnitOfWork
{
    private readonly DataContext db;
    public IArticleRepository ArticleRepository { get; }
    public ICategoryRepository CategoryRepository { get; }
    public ITagRepository TagRepository { get; }
    public IAuthRepository AuthRepository { get; }
    public UnitOfWork(DataContext db, IArticleRepository articleRepository,
        ICategoryRepository categoryRepository, ITagRepository tagRepository,
        IAuthRepository authRepository)
    {
        this.db = db;
        this.ArticleRepository = articleRepository;
        this.CategoryRepository = categoryRepository;
        this.TagRepository = tagRepository;
        this.AuthRepository = authRepository;
    }
    public void Commit() => db.SaveChanges();
}
