﻿using MyNews.DAL.Repositories;

namespace MyNews.DAL.Data;

public interface IUnitOfWork
{
    IArticleRepository ArticleRepository { get; }
    ICategoryRepository CategoryRepository { get; }
    ITagRepository TagRepository { get; }
    IAuthRepository AuthRepository { get; }
    void Commit();
}
